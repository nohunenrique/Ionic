import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  componentes: Componente[] = [
    {
      icon: 'american-football' ,
      name: 'Actions-sheet',
      redirectTo: '/actions-sheet'
    },
    {
      icon: 'add-circle-outline' ,
      name: 'Alert',
      redirectTo: '/alert'
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
interface Componente {
  icon: string;
  name: string;
  redirectTo: string;
}
